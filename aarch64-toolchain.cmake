set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT /arm/sysroot)

set(tools /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu)
set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)
set(CMAKE_Fortran_COMPILER ${tools}/bin/aarch64-linux-gnu-gfortran)

set( CMAKE_C_FLAGS " -isystem /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/include " CACHE STRING "CFLAGS" )
set( CMAKE_C_LINK_FLAGS "" CACHE STRING "LDFLAGS" )

set( CMAKE_CXX_FLAGS " -isystem /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/aarch64-centos7/include/c++/8.3.0 -isystem /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/include " CACHE STRING "CXXFLAGS" )
set( CMAKE_CXX_LINK_FLAGS " /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/aarch64-centos7/lib64/libstdc++.so " CACHE STRING "LDFLAGS" )

set( CMAKE_FIND_ROOT_PATH /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/aarch64-linux-gnu/libc /arm/sysroot )

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)

set(JAVA_JVM_LIBRARY /usr/lib/jvm/java-1.8.0-openjdk/jre/lib/amd64/server/libjvm.so CACHE PATH "Java")
set(JAVA_HOME       /usr/lib/jvm/java-1.8.0-openjdk CACHE PATH "Java")

set(JAVA_AWT_LIBRARY "/arm/sysroot/usr/lib/jvm/java-1.8.0-openjdk/jre/lib/aarch64/libawt.so" CACHE PATH "Java AWT Library")
set(JAVA_JVM_LIBRARY "/arm/sysroot/usr/lib/jvm/java-1.8.0-openjdk/jre/lib/aarch64/server/libjvm.so" CACHE PATH "Java JVM Library")
set(JAVA_INCLUDE_PATH "/arm/sysroot/usr/lib/jvm/java-1.8.0-openjdk/include" CACHE PATH "Java JNI include")
set(JAVA_INCLUDE_PATH2 "/arm/sysroot/usr/lib/jvm/java-1.8.0-openjdk/include/linux" CACHE PATH "Java JNI include")

# Should be set by caller
# set(BINARY_TAG aarch64-centos7-gcc8-opt)
