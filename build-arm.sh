#!/bin/bash
#
# Run this in the area where you can check-out and build the software.
#
# Define BINARY_TAG before calling it to change the build configuration,
# e.g. for a -dbg build.
#
# Specify a different DAY variable if you don't want to use yesterday's
# LCG release, in gitlab e.g. via pipeline variables.
#
# Another useful option is -D CTEST_NO_SUBMIT=1 if you don't want to
# to submit results to CDash or can't (outside of CERN)

export DAY=${DAY:=$(date -d yesterday +%a)}

export BINARY_TAG=${BINARY_TAG:=aarch64-centos7-gcc8-opt}

export LCG_VERSION_CONFIG=${LCG_VERSION_CONFIG:-LCG_999devARM/${DAY}}
export TDAQ_VERSION=${TDAQ_VERSION:-tdaq-99-00-00}
export TDAQ_COMMON_VERSION=${TDAQ_COMMON_VERSION:-tdaq-common-99-00-00}

# No authentication required with this URL
export GITROOT=https://gitlab.cern.ch/atlas-tdaq-software

/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack \
  --tar  \
  --variant=arm \
  ${TDAQ_COMMON_VERSION} ${TDAQ_VERSION} ${BINARY_TAG} \
  -D CMAKE_TOOLCHAIN_FILE=/arm/aarch64-toolchain.cmake -D LCG_VERSION_CONFIG=${LCG_VERSION_CONFIG} -D TDAQ_NO_DEBUG_INSTALL=1 -- \
  -D CTEST_NOTESTS=1
sed -i -e 's;/arm/sysroot;;g' tdaq-common/${TDAQ_COMMON_VERSION}/installed/share/cmake/tdaq-common/${BINARY_TAG}/tdaq-commonTargets.cmake tdaq/${TDAQ_VERSION}/installed/share/cmake/tdaq/${BINARY_TAG}/tdaqTargets.cmake 

