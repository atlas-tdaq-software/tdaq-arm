# Source this file from bash

export TDAQ_RELEASE_BASE=${TDAQ_RELEASE_BASE:=/sw/atlas/tdaq}
export CMTRELEASE=${CMTRELEASE:=tdaq-99-00-00}
export CMTCONFIG=${CMTCONFIG:=aarch64-centos7-gcc8-opt}
export CMAKE_PREFIX_PATH=/sw/atlas/tdaq/tdaq-common/tdaq-common-99-00-00/installed/share/cmake_tdaq/cmake:/sw/atlas/tdaq
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.14.2/Linux-x86_64/bin:$PATH

## For most packages
# /cvmfs/sft.cern.ch/lcg/contrib/CMake/3.14.2/Linux-x86_64/bin/cmake -D CMAKE_TOOLCHAIN_FILE=/arm/aarch64-toolchain.cmake ..

cmake_config()
{
   mkdir -p $CMTCONFIG
   (cd $CMTCONFIG && cmake -D CMAKE_TOOLCHAIN_FILE=/arm/aarch64-toolchain.cmake ..) 
}
