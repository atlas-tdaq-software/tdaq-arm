# tdaq-arm

The build scripts and Dockerfile to customize the ARM cross-compiler image for ATLAS TDAQ

Manual release build
---------------------

Look at `build.sh` and `build-arm.sh` to see how the release is built inside the container.
To do it manually:

```bash
docker run --rm -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/arm-build-tdaq
mkdir /build
cd /build
/bin/build-arm.sh
```

Using the CVMFS installation
-----------------------------

You can also use  this image to develop against the CVMFS nightly installation:

```bash
docker run --rm -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq-arm/arm-build-tdaq
export TDAQ_RELEASE_BASE=/cvmfs/atlas-online-nightlies.cern.ch/tdaq/arm
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh -b tdaq-99-00-00 aarch64-centos7-gcc8-opt
export CMTRELEASE=tdaq-99-00-00
export CMTCONFIG=aarch64-centos7-gcc8-opt
# work area
mkdir /work
cd /work
cp /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/cmake/templates/CMakeLists.txt .
# package of your choice
git clone http://gitlab.cern.ch/atlas-tdaq-software/owl.git
mkdir build
cd build
cmake -D CMAKE_TOOLCHAIN_FILE=/arm/aarch64-toolchain.cmake ..
make 
```


Using the cross-compiler image
------------------------------

```bash
docker run --rm -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq-arm/tdaq-arm
% source /sw/atlas/setup-user.sh
% mkdir work && cd work
% cp /sw/atlas/CMakeLists.txt .
% git clone ...
% cmake_config
```

Using the native image
-----------------------

```bash
docker run --rm -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq-arm/tdaq-arm-native
% /sw/atlas/fixup_targets.sh
% export TDAQ_RELEASE_BASE=/sw/atlas/tdaq
% source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-99-00-00
% source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8.3.0/aarch64-centos7/setup.sh
% export PATH=/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/devARM/Tue/CMake/3.14.3/aarch64-centos7-gcc8-opt/bin:$PATH
% mkdir work
% cd work
% git clone ...
% cmake_config
% cd $CMTCONFIG
% make
# To execute
% export TDAQ_IPC_INIT_REF=file:/tmp/init.ref
% ipc_server &
% ipc_ls -l
```

Using the native run-time only image
-------------------------------------

The only difference to the previous image is that absence of several -devel RPMs. 

```bash
docker run --rm -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq-arm/tdaq-arm-runtime
% source /sw/atlas/tdaq/tdaq/tdaq-99-00-00/installed/setup.sh
% source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8.3.0/aarch64-centos7/setup.sh
% export TDAQ_IPC_INIT_REF=file:/tmp/init.ref
% ipc_server &
% ipc_ls -l
```
