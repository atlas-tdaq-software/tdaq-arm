FROM gitlab-registry.cern.ch/atlas-tdaq-software/arm-cross-compiler/arm-cross-build:latest
COPY build-arm.sh /usr/bin/
COPY aarch64-toolchain.cmake /arm/aarch64-toolchain.cmake
