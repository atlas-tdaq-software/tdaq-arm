all:	install

prepare:
	docker build --pull -t gitlab-registry.cern.ch/atlas-tdaq-software/tdaq-arm/arm-build-tdaq -f Dockerfile .

build:	prepare
	mkdir -p output
	docker run --rm -v `echo -n $(KRB5CCNAME) | sed 's;FILE:;;'`:/tmp/krb5_root -e KRB5CCNAME=FILE:/tmp/krb5_root -v /cvmfs:/cvmfs:ro,shared -v `pwd`/output:/output gitlab-registry.cern.ch/atlas-tdaq-software/tdaq-arm/arm-build-tdaq /bin/build.sh

install: build
	docker build --pull -v `pwd`/output:/output -t gitlab-registry.cern.ch/atlas-tdaq-software/tdaq-arm/tdaq-arm -f Dockerfile.install .
